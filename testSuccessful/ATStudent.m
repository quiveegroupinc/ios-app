//
//  ATStudent.m
//  32 tableViewEditingPartTwo
//
//  Created by Aleksandr Tsebrii on 1/29/16.
//  Copyright © 2016 Aleksandr Tsebrii. All rights reserved.
//

#import "ATStudent.h"

@implementation ATStudent

+ (ATStudent *)randomStudent {
    
    ATStudent *student = [[ATStudent alloc] init];
    NSArray *firstNames = [[NSArray alloc] initWithObjects:
                           @"Noah", @"Emma", @"Liam", @"Olivia", @"Mason", @"Sophia", @"Jacob", @"Isabella", @"William", @"Ava",
                           @"Ethan", @"Mia", @"Michael", @"Emily", @"Alexander", @"Abigail", @"James", @"Madison", @"Daniel", @"Charlotte",
                           @"Elijah", @"Harper", @"Benjamin", @"Sofia", @"Logan", @"Avery", @"Aiden", @"Elizabeth", @"Jayden", @"Amelia",
                           @"Matthew", @"Evelyn", @"Jackson", @"Ella", @"David", @"Chloe", @"Lucas", @"Victoria", @"Joseph", @"Aubrey",
                           @"Anthony", @"Grace", @"Andrew", @"Zoey", @"Samuel", @"Natalie", @"Gabriel", @"Addison", @"Joshua", @"Lillian",
                           @"John", @"Brooklyn", @"Carter", @"Lily", @"Luke", @"Hannah", @"Dylan", @"Layla", @"Christophe", @"Scarlett",
                           @"Isaac", @"Aria", @"Oliver", @"Zoe", @"Henry", @"Samantha", @"Sebastian", @"Anna", @"Caleb", @"Leah",
                           @"Owen", @"Audrey", @"Ryan", @"Ariana", @"Nathan", @"Allison", @"Wyatt", @"Savannah", @"Hunter", @"Arianna",
                           @"Jack", @"Camila", @"Christian", @"Penelope", @"Landon", @"Gabriella", @"Jonathan", @"Claire", @"Levi", @"Aaliyah",
                           @"Jaxon", @"Sadie", @"Julian", @"Riley", @"Isaiah", @"Skylar", @"Eli", @"Nora", @"Aaron", @"Sarah", nil];
    NSArray *lastNames = [[NSArray alloc] initWithObjects:
                          @"SMITH", @"JOHNSON", @"WILLIAMS", @"BROWN", @"JONES", @"MILLER", @"DAVIS", @"GARCIA", @"RODRIGUEZ", @"WILSON",
                          @"MARTINEZ", @"ANDERSON", @"TAYLOR", @"THOMAS", @"HERNANDEZ", @"MOORE", @"MARTIN", @"JACKSON", @"THOMPSON", @"WHITE",
                          @"LOPEZ", @"LEE", @"GONZALEZ", @"HARRIS", @"CLARK", @"LEWIS", @"ROBINSON", @"WALKER", @"PEREZ", @"HALL",
                          @"YOUNG", @"ALLEN", @"SANCHEZ", @"WRIGHT", @"KING", @"SCOTT", @"GREEN", @"BAKER", @"ADAMS", @"NELSON",
                          @"HILL", @"RAMIREZ", @"CAMPBELL", @"MITCHELL", @"ROBERTS", @"CARTER", @"PHILLIPS", @"EVANS", @"TURNER", @"TORRES",
                          @"PARKER", @"COLLINS", @"EDWARDS", @"STEWART", @"FLORES", @"MORRIS", @"NGUYEN", @"MURPHY", @"RIVERA", @"COOK",
                          @"ROGERS", @"MORGAN", @"PETERSON", @"COOPER", @"REED", @"BAILEY", @"BELL", @"GOMEZ", @"KELLY", @"HOWARD",
                          @"WARD", @"COX", @"DIAZ", @"RICHARDSON", @"WOOD", @"WATSON", @"BROOKS", @"BENNETT", @"GRAY", @"JAMES",
                          @"REYES", @"CRUZ", @"HUGHES", @"PRICE", @"MYERS", @"LONG", @"FOSTER", @"SANDERS", @"ROSS", @"MORALES",
                          @"POWELL", @"SULLIVAN", @"RUSSELL", @"ORTIZ", @"JENKINS", @"GUTIERREZ", @"PERRY", @"BUTLER", @"BARNES", @"FISHER",nil];
    student.firstName = [firstNames objectAtIndex:arc4random() % 50];
    student.lastName = [lastNames objectAtIndex:arc4random() % 50];
    student.averageGrade = ((CGFloat)((arc4random() % 301) + 200)) / 100;
    return student;
    
}

@end
