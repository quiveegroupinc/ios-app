//
//  ATUser.m
//  testSuccessful
//
//  Created by Aleksandr Tsebrii on 5/7/16.
//  Copyright © 2016 Aleksandr Tsebrii. All rights reserved.
//

#import "ATUser.h"

@implementation ATUser

+ (ATUser *)sharedUser {
    
    static ATUser *sharedUser = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUser = [[ATUser alloc] init];
    });
    
    return sharedUser;
    
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.identifier = 0;
        self.points = 0;
        self.experience = 0;
    }
    
    return self;
}

@end
