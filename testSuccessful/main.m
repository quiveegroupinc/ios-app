//
//  main.m
//  testSuccessful
//
//  Created by Aleksandr Tsebrii on 5/6/16.
//  Copyright © 2016 Aleksandr Tsebrii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
