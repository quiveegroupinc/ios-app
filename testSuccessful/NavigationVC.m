//
//  NavigationVC.m
//  Menu
//
//  Created by Alex Sorokolita on 08.05.16.
//  Copyright © 2016 Alexandr Sorokolita. All rights reserved.
//

#import "NavigationVC.h"
#import "MenuVC.h"
#import "UIViewController+REFrostedViewController.h"

@interface NavigationVC ()

@end

@implementation NavigationVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)]];
}

- (void)showMenu
{
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    [self.frostedViewController presentMenuViewController];
}

#pragma mark -
#pragma mark Gesture recognizer

- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender
{
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    [self.frostedViewController panGestureRecognized:sender];
}


@end
