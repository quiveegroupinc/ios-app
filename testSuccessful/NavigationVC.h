//
//  NavigationVC.h
//  Menu
//
//  Created by Alex Sorokolita on 08.05.16.
//  Copyright © 2016 Alexandr Sorokolita. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationVC : UINavigationController

- (void)showMenu;

@end
