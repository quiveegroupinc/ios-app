//
//  MenuVC.m
//  Menu
//
//  Created by Alex Sorokolita on 08.05.16.
//  Copyright © 2016 Alexandr Sorokolita. All rights reserved.
//

#import "MenuVC.h"
#import "SecondVC.h"
#import "NavigationVC.h"
#import "UIViewController+REFrostedViewController.h"

@interface MenuVC ()

@end

@implementation MenuVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

#pragma mark IBActions

- (IBAction)myTasksAction:(id)sender {
    
    SecondVC *secondVC = [[SecondVC alloc] init];
    NavigationVC *navigationVC = [[NavigationVC alloc] initWithRootViewController:secondVC];
    self.frostedViewController.contentViewController = navigationVC;
    
    [self.frostedViewController hideMenuViewController];
}

- (IBAction)progressAction:(id)sender {
}

- (IBAction)enjoyAction:(id)sender {
}

- (IBAction)settingsAction:(id)sender {
}

- (IBAction)logoutAction:(id)sender {
}
- (IBAction)addNewTaskAction:(id)sender {
}

@end
