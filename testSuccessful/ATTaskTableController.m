//
//  ATTaskTableController.m
//  testSuccessful
//
//  Created by Aleksandr Tsebrii on 5/7/16.
//  Copyright © 2016 Aleksandr Tsebrii. All rights reserved.
//

#import "ATTaskTableController.h"
#import "ATTask.h"
#import "ATGroup.h"
#import "NavigationVC.h"
#import "SESlideTableViewCell.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface ATTaskTableController () <SESlideTableViewCellDelegate>

@property (strong, nonatomic) NSArray *groupsArray;
@property (strong, nonatomic) NSString *inputString;

@end

@implementation ATTaskTableController

#pragma mark - UIViewController

- (void)loadView {
    
    [super loadView];
    
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:(NavigationVC *)self.navigationController
                                                                     action:@selector(showMenu)];
    menuBarButton.image = [UIImage imageNamed:@"menu_btn"];
    
    UIBarButtonItem *addBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                  target:self
                                                                                  action:@selector(actionAddRow:)];
    self.navigationItem.leftBarButtonItem = menuBarButton;
    self.navigationItem.rightBarButtonItem = addBarButton;
    
    UIImage *logo = [UIImage imageNamed:@"navLogo"];
    UIImageView *logoView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    logoView.image = logo;
    logoView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = logoView;
    
    self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x903382);
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ATGroup *group = [[ATGroup alloc] init];
    self.groupsArray = @[group];
    
    [self.tableView reloadData];
    
}

#pragma mark - Actions

- (void)actionEdit:(UIBarButtonItem *)sender {
    
    BOOL isEditing = self.tableView.editing;
    [self.tableView setEditing:!isEditing
                      animated:YES];
    
    UIBarButtonSystemItem item = self.tableView.editing ? UIBarButtonSystemItemDone : UIBarButtonSystemItemEdit;
    UIBarButtonItem *editBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:item
                                                                                   target:self
                                                                                   action:@selector(actionEdit:)];
    [self.navigationItem setLeftBarButtonItem:editBarButton
                                     animated:YES];
    
}

- (void)actionAddRow:(UIBarButtonItem *)sender {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"alertTitle"
                                          message:@"alertMessage"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = NSLocalizedString(@"Text", @"Text");
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
     }];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                                   
                                   ATGroup *anyGroup = [self.groupsArray firstObject];
                                   
                                   
                                   
                                   NSMutableArray *temporaryTasksArray = [[NSMutableArray alloc] initWithArray:anyGroup.tasksArray];
                                   
                                   NSInteger index = 0;
                                   NSString *newString = self.inputString;
                                   [temporaryTasksArray insertObject:newString atIndex:index];
                                   anyGroup.tasksArray = temporaryTasksArray;
                                   
                                   [self.tableView beginUpdates];
                                   NSIndexPath *insertRow = [NSIndexPath indexPathForItem:index inSection:0];
                                   [self.tableView insertRowsAtIndexPaths:[[NSArray alloc] initWithObjects:insertRow, nil] withRowAnimation:UITableViewRowAnimationTop];
                                   [self.tableView endUpdates];
                                   
                               }];
    
    okAction.enabled = NO;
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        UITextField *login = alertController.textFields.firstObject;
        UIAlertAction *okAction = alertController.actions.lastObject;
        okAction.enabled = login.text.length > 2;
        self.inputString = sender.text;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.groupsArray.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    ATGroup *group = [self.groupsArray objectAtIndex:section];
    return group.tasksArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    static NSString *studentIdentifier = @"addIdentifier";
    //    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:studentIdentifier];
    //
    //    if (!cell) {
    //
    //        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
    //                                      reuseIdentifier:studentIdentifier];
    //
    //    }
    
    NSString *const Cell = @"Cell";
    
    SESlideTableViewCell *cell = (SESlideTableViewCell*)[tableView dequeueReusableCellWithIdentifier:Cell];
    if (cell == nil) {
        cell = [[SESlideTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cell];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        
        [cell addRightButtonWithImage:[UIImage imageNamed:@"swipeEdit"] backgroundColor:UIColorFromRGB(0xf6f0f5)];
        [cell addRightButtonWithImage:[UIImage imageNamed:@"swipeDelete"] backgroundColor:UIColorFromRGB(0xf6f0f5)];
        [cell addRightButtonWithImage:[UIImage imageNamed:@"swipeDone"] backgroundColor:UIColorFromRGB(0xf6f0f5)];
    }
    
    ATGroup *group = [self.groupsArray objectAtIndex:indexPath.section];
    
    NSString *string = [group.tasksArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = string;
    
    return cell;
    
}

- (void)slideTableViewCell:(SESlideTableViewCell*)cell didTriggerRightButton:(NSInteger)buttonIndex {
    
    NSLog(@"right button triggered:%d", (int)buttonIndex);
    [cell updateContentViewSnapshot];
    [cell setSlideState:SESlideTableViewCellSlideStateCenter animated:YES];
    
    if (buttonIndex == 0) {
        
    }
    
    if (buttonIndex == 1) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        
        ATGroup *group = [self.groupsArray objectAtIndex:indexPath.section];
        NSMutableArray *temporaryStudentsArray = [[NSMutableArray alloc] initWithArray:group.tasksArray];
        [temporaryStudentsArray removeObjectAtIndex:indexPath.row];
        group.tasksArray = temporaryStudentsArray;
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:[[NSArray alloc] initWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationBottom];
        [self.tableView endUpdates];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [self.tableView reloadData];
            }
        });
    }
    
    if (buttonIndex == 2) {
        cell.backgroundColor = [UIColor greenColor];
    }
}

//- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    return YES;
//
//}
//
//- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
//
//    ATGroup *sourceGroup = [self.groupsArray objectAtIndex:sourceIndexPath.section];
//
//    NSMutableArray *temporaryStudentsArray = [[NSMutableArray alloc] initWithArray:sourceGroup.tasksArray];
//
//    [temporaryStudentsArray exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
//
//    sourceGroup.tasksArray = temporaryStudentsArray;
//
//    [self.tableView reloadData];
//
//}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//
//        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
//
//        ATGroup *group = [self.groupsArray objectAtIndex:indexPath.section];
//        //ATTask *task = [group.tasksArray objectAtIndex:indexPath.row];
//        NSMutableArray *temporaryStudentsArray = [[NSMutableArray alloc] initWithArray:group.tasksArray];
//        [temporaryStudentsArray removeObjectAtIndex:indexPath.row];
//        group.tasksArray = temporaryStudentsArray;
//        [tableView beginUpdates];
//        [tableView deleteRowsAtIndexPaths:[[NSArray alloc] initWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationBottom];
//        [tableView endUpdates];
//
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
//                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//                [tableView reloadData];
//            }
//        });
//
//    }
//
//}

//#pragma mark - UITableViewDelegate
//
//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    return YES;
//
//}
//
//- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    return NO;
//
//}
//
//- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
//
//    return proposedDestinationIndexPath.row == 0 ? sourceIndexPath : proposedDestinationIndexPath;
//
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//
//}
//
//- (nullable NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    return @"Remove";
//    
//}

@end