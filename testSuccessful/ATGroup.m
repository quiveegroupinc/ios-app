//
//  ATGroup.m
//  32 tableViewEditingPartTwo
//
//  Created by Aleksandr Tsebrii on 1/29/16.
//  Copyright © 2016 Aleksandr Tsebrii. All rights reserved.
//

#import "ATGroup.h"
#import "ATTask.h"

@implementation ATGroup

- (instancetype)init {
    self = [super init];
    if (self) {
        self.tasksArray = @[@"Wake up and express",
                            @"Mentally walk through my priorities for the day.",
                            @"Head down to the home gym",
                            @"Wake the kids",
                            @"Get dressed",
                            @"Head to the office",
                            @"Skim the newspaper",
                            @"Send at least two networking notes",
                            @"Write down the priorities for the day",
                            @" Start working on that list"];
    }
    return self;
}

@end
