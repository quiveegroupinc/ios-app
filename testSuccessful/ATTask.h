//
//  ATStudent.h
//  32 tableViewEditingPartTwo
//
//  Created by Aleksandr Tsebrii on 1/29/16.
//  Copyright © 2016 Aleksandr Tsebrii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ATTask : NSObject

@property (assign, nonatomic) NSInteger identifier;
@property (strong, nonatomic) NSString *text;
@property (assign, nonatomic) BOOL status;
@property (assign, nonatomic) NSInteger level;

@end