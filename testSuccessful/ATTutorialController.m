//
//  ATTutorialController.m
//  testSuccessful
//
//  Created by Aleksandr Tsebrii on 5/7/16.
//  Copyright © 2016 Aleksandr Tsebrii. All rights reserved.
//

#import "ATTutorialController.h"
#import "ATLoginController.h"

@implementation ATTutorialController

#pragma mark - Lifecycle

- (void)loadView {
    [super loadView];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *skipButton = [UIButton buttonWithType:UIButtonTypeSystem];
    skipButton.frame = CGRectMake(CGRectGetMinX(self.view.bounds),
                                  CGRectGetMaxY(self.view.bounds) - CGRectGetHeight(self.view.bounds) * 0.1f,
                                  CGRectGetWidth(self.view.bounds),
                                  CGRectGetHeight(self.view.bounds) * 0.1f);
    [skipButton setTitle:@"Skip" forState:UIControlStateNormal];
    [self.view addSubview:skipButton];
    [skipButton addTarget:self
                   action:@selector(actionSkipButton:)
         forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Tutorial";
    
}

#pragma mark - Action

- (void)actionSkipButton:(UIButton *)sender {
    
    ATLoginController *loginController = [[ATLoginController alloc] init];
    [self.navigationController pushViewController:loginController animated:YES];
    
}

@end
