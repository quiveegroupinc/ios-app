//
//  ATUser.h
//  testSuccessful
//
//  Created by Aleksandr Tsebrii on 5/7/16.
//  Copyright © 2016 Aleksandr Tsebrii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ATUser : NSObject

@property (assign, nonatomic) NSInteger *identifier;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *password;
@property (assign, nonatomic) NSInteger *points;
@property (assign, nonatomic) NSInteger *experience;

@property (assign, nonatomic) NSString *accessToken;
@property (assign, nonatomic) NSString *authKye;

+ (ATUser *)sharedUser;

@end
