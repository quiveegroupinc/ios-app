//
//  ATLoginController.m
//  testSuccessful
//
//  Created by Aleksandr Tsebrii on 5/7/16.
//  Copyright © 2016 Aleksandr Tsebrii. All rights reserved.
//

#import "ATLoginController.h"
#import "ATUser.h"
#import "ATTaskTableController.h"

@interface ATLoginController () <UITextFieldDelegate>

@property (weak, nonatomic) UITextField *emailTextField;
@property (weak, nonatomic) UITextField *passwordTextField;

@end

@implementation ATLoginController

#pragma mark - Lifecycle

- (void)loadView {
    [super loadView];
    
    UITextField *emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.view.bounds),
                                                                                CGRectGetMidY(self.view.bounds) - (CGRectGetHeight(self.view.bounds) * 0.1f) * 2.f,
                                                                                CGRectGetWidth(self.view.bounds),
                                                                                CGRectGetHeight(self.view.bounds) * 0.1f)];
    emailTextField.placeholder = @"Ener email";
    emailTextField.textAlignment = NSTextAlignmentCenter;
    emailTextField.borderStyle = UITextBorderStyleRoundedRect;
    emailTextField.delegate = self;
    [self.view addSubview:emailTextField];
    self.emailTextField = emailTextField;
    
    UITextField *passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.view.bounds),
                                                                                CGRectGetMidY(self.view.bounds) - CGRectGetHeight(self.view.bounds) * 0.1f,
                                                                                CGRectGetWidth(self.view.bounds),
                                                                                CGRectGetHeight(self.view.bounds) * 0.1f)];
    passwordTextField.placeholder = @"Ener email";
    passwordTextField.textAlignment = NSTextAlignmentCenter;
    passwordTextField.borderStyle = UITextBorderStyleRoundedRect;
    passwordTextField.delegate = self;
    [self.view addSubview:passwordTextField];
    self.passwordTextField = passwordTextField;
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeSystem];
    loginButton.frame = CGRectMake(CGRectGetMinX(self.view.bounds),
                                  CGRectGetMidY(self.view.bounds) + CGRectGetHeight(self.view.bounds) * 0.1f,
                                  CGRectGetWidth(self.view.bounds),
                                  CGRectGetHeight(self.view.bounds) * 0.1f);
    [loginButton setTitle:@"Skip" forState:UIControlStateNormal];
    [self.view addSubview:loginButton];
    [loginButton addTarget:self
                   action:@selector(actionLoginButton:)
         forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Login";
    
}

#pragma mark - Action

- (IBAction)actionLoginButton:(UIButton *)sender {
    
    
    if (self.emailTextField.text.length > 0 || self.emailTextField.text != nil || ![self.emailTextField.text isEqual:@""]) {
        
//        [ATUser sharedUser].email = self.emailTextField.text;
        
    }
    
    if (self.passwordTextField.text.length > 0 || self.passwordTextField.text != nil || ![self.passwordTextField.text isEqual:@""]) {
        
//        [ATUser sharedUser].password = self.passwordTextField.text;
        
    }
    
    ATTaskTableController *taskTableController = [[ATTaskTableController alloc] init];
    [self.navigationController pushViewController:taskTableController animated:YES];
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:self.emailTextField]) {
        [self.passwordTextField becomeFirstResponder];
    } else if ([textField isEqual:self.passwordTextField]) {
        [textField resignFirstResponder];
    }
    
    return NO;
    
}

@end
