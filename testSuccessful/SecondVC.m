//
//  SecondVC.m
//  Menu
//
//  Created by Alex Sorokolita on 08.05.16.
//  Copyright © 2016 Alexandr Sorokolita. All rights reserved.
//

#import "SecondVC.h"
#import "NavigationVC.h"

@interface SecondVC ()

@end

@implementation SecondVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"SecondVC";
    self.view.backgroundColor = [UIColor orangeColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:(NavigationVC *)self.navigationController
                                                                            action:@selector(showMenu)];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

@end
