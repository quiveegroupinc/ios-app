//
//  AppDelegate.h
//  testSuccessful
//
//  Created by Aleksandr Tsebrii on 5/6/16.
//  Copyright © 2016 Aleksandr Tsebrii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
