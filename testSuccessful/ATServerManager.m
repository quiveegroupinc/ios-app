//
//  ATServerManager.m
//  testSuccessful
//
//  Created by Aleksandr Tsebrii on 5/7/16.
//  Copyright © 2016 Aleksandr Tsebrii. All rights reserved.
//

//#import "ATServerManager.h"
//#import "AFNetworking.h"
//#import "ATCourse.h"
//
//@interface ATServerManager ()
//
//@property (strong, nonatomic) AFHTTPSessionManager *sessionManager;
//
//@end
//
//@implementation ATServerManager
//
//- (instancetype)init {
//    self = [super init];
//    
//    if (self) {
//        
//        NSURL *baseURL = [NSURL URLWithString:@"https://api..."];
//        self.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
//        
//    }
//    
//    return self;
//    
//}
//
//+ (ATServerManager *)sharedManager {
//    
//    static ATServerManager *serverManager = nil;
//    
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        serverManager = [[ATServerManager alloc] init];
//    });
//    
//    return serverManager;
//    
//}
//
//- (void)getCashCourseOnSuccess:(void(^)(NSArray *courses))success
//                     onFailure:(void(^)(NSError *error))failure {
//    
//    [self.sessionManager GET:@"pubinfo?json..."
//                  parameters:nil
//                    progress:nil
//                     success:^(NSURLSessionTask *task, id responseObject) {
//                         
//                         NSLog(@"JSON: %@", responseObject);
//                         
//                         NSMutableArray *courses = [[NSMutableArray alloc] init];
//                         for (NSDictionary *currentDictionary in responseObject) {
//                             ATCourse *course = [[ATCourse alloc] initWithServerResponse:currentDictionary];
//                             [courses addObject:course];
//                         }
//                         
//                         if (success) {
//                             success(courses);
//                         }
//                         
//                     } failure:^(NSURLSessionTask *operation, NSError *error) {
//                         
//                         NSLog(@"Error: %@", error);
//                         
//                         if (failure) {
//                             failure(error);
//                         }
//                         
//                     }];
//    
//}
//
//@end
