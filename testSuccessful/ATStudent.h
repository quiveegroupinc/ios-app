//
//  ATStudent.h
//  32 tableViewEditingPartTwo
//
//  Created by Aleksandr Tsebrii on 1/29/16.
//  Copyright © 2016 Aleksandr Tsebrii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ATStudent : NSObject

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (assign, nonatomic) CGFloat averageGrade;

+ (ATStudent *)randomStudent;

@end